package org.example;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PacmanGame extends JPanel implements ActionListener, KeyListener {
  private int pacmanX = 100;
  private int pacmanY = 100;
  private int pacSize = 45;
  private int pacSpeed = 5;
  private Timer timer;
  private int direction = KeyEvent.VK_RIGHT;
  private int ghostDirection = 0;

  private int angle = 45;
  private int score = 0;
  private int level = 1;
  private int totalCoins = 10;

  private int gameWidth = 800;
  private int gameHeight = 600;

  private List<Point> smallCoins = new ArrayList<>();
  private List<Point> largeCoins = new ArrayList<>();
  private List<Rectangle2D> walls = new ArrayList<>();
  private Random random = new Random();

  private Point ghost1 = new Point(0, 550);
  private Point ghost2 = new Point(750, 550);
  private Point ghost3 = new Point(400, 550);
  private int lives = 3;
  private boolean isGameOver = false;

  public PacmanGame() {
    this.setPreferredSize(new Dimension(gameWidth, gameHeight));
    this.setFocusable(true);
    this.addKeyListener(this);
    timer = new Timer(50, this);
    timer.start();
    createWalls();
    spawnCoins();
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2d = (Graphics2D) g;
    g2d.setColor(Color.BLACK);
    g2d.fillRect(0, 0, gameWidth, gameHeight);
    g2d.setColor(Color.PINK);
    for (Rectangle2D wall : walls) {
      g2d.draw(wall);
    }
    g2d.setColor(Color.YELLOW);
    g2d.fillArc(pacmanX, pacmanY, pacSize, pacSize, angle, 270);
    g2d.setColor(Color.RED);
    for (Point point : smallCoins) {
      g2d.fillOval(point.x, point.y, 10, 10);
    }
    g2d.setColor(Color.ORANGE);
    for (Point point : largeCoins) {
      g2d.fillOval(point.x, point.y, 20, 20);
    }
    g2d.setColor(Color.BLUE);
    g2d.fillOval(ghost1.x, ghost1.y, pacSize, pacSize);
    g2d.setColor(Color.GREEN);
    g2d.fillOval(ghost2.x, ghost2.y, pacSize, pacSize);
    g2d.setColor(Color.WHITE);
    g2d.fillOval(ghost3.x, ghost3.y, pacSize, pacSize);
    g2d.setColor(Color.WHITE);
    g2d.setFont(new Font("Arial", Font.PLAIN, 20));
    g2d.drawString("Level: " + level, 20, 20);
    g2d.drawString("Score: " + score, 20, 50);
    g2d.drawString("Lives: " + lives, 20, 80);
    if (isGameOver) {
      g2d.setFont(new Font("Arial", Font.BOLD, 50));
      g2d.setColor(Color.RED);
      g2d.drawString("Game Over", gameWidth / 2 - 150, gameHeight / 2);
    }
  }

  public void actionPerformed(ActionEvent e) {
    if (!isGameOver) {
      movePacman();
      moveGhostRandomly(ghost1);
      moveGhostRandomly(ghost2);
      moveGhostRandomly(ghost3);
      checkCollisions();
      repaint();
    }
  }

  public void keyPressed(KeyEvent e) {
    int key = e.getKeyCode();
    int newDirection = direction;

    if (key == KeyEvent.VK_LEFT) {
      newDirection = KeyEvent.VK_LEFT;
    } else if (key == KeyEvent.VK_RIGHT) {
      newDirection = KeyEvent.VK_RIGHT;
    } else if (key == KeyEvent.VK_UP) {
      newDirection = KeyEvent.VK_UP;
    } else if (key == KeyEvent.VK_DOWN) {
      newDirection = KeyEvent.VK_DOWN;
    }

    int newPacmanX = pacmanX;
    int newPacmanY = pacmanY;

    switch (newDirection) {
      case KeyEvent.VK_LEFT:
        newPacmanX = pacmanX - pacSpeed;
        break;
      case KeyEvent.VK_RIGHT:
        newPacmanX = pacmanX + pacSpeed;
        break;
      case KeyEvent.VK_UP:
        newPacmanY = pacmanY - pacSpeed;
        break;
      case KeyEvent.VK_DOWN:
        newPacmanY = pacmanY + pacSpeed;
        break;
    }

    Rectangle2D pacmanBounds = new Rectangle2D.Double(newPacmanX, newPacmanY, pacSize, pacSize);
    boolean collidedWithWall = false;

    for (Rectangle2D wall : walls) {
      if (pacmanBounds.intersects(wall)) {
        collidedWithWall = true;
        break;
      }
    }

    if (!collidedWithWall) {
      direction = newDirection;
    }
  }

  public void keyTyped(KeyEvent e) {
  }

  public void keyReleased(KeyEvent e) {
  }

  private void movePacman() {
    int newPacmanX = pacmanX;
    int newPacmanY = pacmanY;

    switch (direction) {
      case KeyEvent.VK_LEFT:
        newPacmanX = pacmanX - pacSpeed;
        angle = -135;
        break;
      case KeyEvent.VK_RIGHT:
        newPacmanX = pacmanX + pacSpeed;
        angle = 45;
        break;
      case KeyEvent.VK_UP:
        newPacmanY = pacmanY - pacSpeed;
        angle = 135;
        break;
      case KeyEvent.VK_DOWN:
        newPacmanY = pacmanY + pacSpeed;
        angle = 315;
        break;
    }

    Rectangle2D pacmanBounds = new Rectangle2D.Double(newPacmanX, newPacmanY, pacSize, pacSize);
    boolean collidedWithWall = false;

    for (Rectangle2D wall : walls) {
      if (pacmanBounds.intersects(wall)) {
        collidedWithWall = true;
        break;
      }
    }

    if (newPacmanX >= 0 && newPacmanX + pacSize <= gameWidth && newPacmanY >= 0 && newPacmanY + pacSize <= gameHeight && !collidedWithWall) {
      pacmanX = newPacmanX;
      pacmanY = newPacmanY;
    }
  }

  private void  moveGhostRandomly(Point ghost) {
    int newGhostX = ghost.x;
    int newGhostY = ghost.y;


    int pacmanCenterX = pacmanX + pacSize / 2;
    int pacmanCenterY = pacmanY + pacSize / 2;


    int ghostCenterX = ghost.x + pacSize / 2;
    int ghostCenterY = ghost.y + pacSize / 2;


    int dx = pacmanCenterX - ghostCenterX;
    int dy = pacmanCenterY - ghostCenterY;


    if (Math.abs(dx) > Math.abs(dy)) {
      if (dx > 0) {

        newGhostX += pacSpeed;
        ghostDirection = 0;
      } else {

        newGhostX -= pacSpeed;
        ghostDirection = 1;
      }
    } else {
      if (dy > 0) {

        newGhostY += pacSpeed;
        ghostDirection = 2;
      } else {

        newGhostY -= pacSpeed;
        ghostDirection = 3;
      }
    }



    if (!canGhostMove(newGhostX, newGhostY)) {
      newGhostX = ghost.x;
      newGhostY = ghost.y;
      switch (ghostDirection) {
        case 0:
          newGhostX += pacSpeed;
          break;
        case 1:
          newGhostX -= pacSpeed;
          break;
        case 2:
          newGhostY += pacSpeed;
          break;
        case 3:
          newGhostY -= pacSpeed;
          break;
      }
    }


    else  {
      ghost.setLocation(newGhostX, newGhostY);
    }
  }


  private boolean canGhostMove(int x, int y) {
    Rectangle2D ghostBounds = new Rectangle2D.Double(x, y, pacSize, pacSize);
    for (Rectangle2D wall : walls) {
      if (ghostBounds.intersects(wall)) {
        return false;
      }
    }
    return true;
  }

  private void checkCollisions() {
    List<Point> smallCoinsToRemove = new ArrayList<>();
    List<Point> largeCoinsToRemove = new ArrayList<>();

    for (Point point : smallCoins) {
      Rectangle2D pacmanBounds = new Rectangle2D.Double(pacmanX, pacmanY, pacSize, pacSize);
      Rectangle2D pointBounds = new Rectangle2D.Double(point.x, point.y, 10, 10);

      if (pacmanBounds.intersects(pointBounds)) {
        smallCoinsToRemove.add(point);
        score += 10;
      }
    }

    for (Point point : largeCoins) {
      Rectangle2D pacmanBounds = new Rectangle2D.Double(pacmanX, pacmanY, pacSize, pacSize);
      Rectangle2D pointBounds = new Rectangle2D.Double(point.x, point.y, 20, 20);

      if (pacmanBounds.intersects(pointBounds)) {
        largeCoinsToRemove.add(point);
        score += 50;
      }
    }

    smallCoins.removeAll(smallCoinsToRemove);
    largeCoins.removeAll(largeCoinsToRemove);

    if (smallCoins.isEmpty() && largeCoins.isEmpty()) {

      level++;
      totalCoins += 5;
      createWalls();
      spawnCoins();
    }


    Rectangle2D ghostBounds = new Rectangle2D.Double(ghost1.x, ghost1.y, pacSize, pacSize);
    Rectangle2D ghostBounds2 = new Rectangle2D.Double(ghost2.x, ghost2.y, pacSize, pacSize);
    Rectangle2D ghostBounds3 = new Rectangle2D.Double(ghost3.x, ghost3.y, pacSize, pacSize);
    Rectangle2D pacmanBounds = new Rectangle2D.Double(pacmanX, pacmanY, pacSize, pacSize);

    if (ghostBounds.intersects(pacmanBounds)) {
      lives--;
      if (lives == 0) {
        isGameOver = true;
      } else {

        pacmanX = 100;
        pacmanY = 100;
        ghost1.setLocation(0, 550);
        ghost2.setLocation(750, 550);
        ghost3.setLocation(400, 550);
      }
    }
  }

  private void spawnCoins() {
    smallCoins.clear();
    largeCoins.clear();

    for (int i = 0; i < totalCoins; i++) {
      int coinX, coinY;
      do {
        coinX = random.nextInt(gameWidth - 10);
        coinY = random.nextInt(gameHeight - 10);
      } while (isPointIntersectingPacman(coinX, coinY) || isPointIntersectingWall(coinX, coinY));

      if (i % 3 == 0) {
        largeCoins.add(new Point(coinX, coinY));
      } else {
        smallCoins.add(new Point(coinX, coinY));
      }
    }
  }

  private void createWalls() {
    walls.clear();

    walls.add(new Rectangle2D.Double(50, 50, 50, 50));
    walls.add(new Rectangle2D.Double(150, 0, 50, 100));
    walls.add(new Rectangle2D.Double(250, 0, 50, 200));
    walls.add(new Rectangle2D.Double(350, 50, 50, 150));
    walls.add(new Rectangle2D.Double(450, 0, 100, 200));
    walls.add(new Rectangle2D.Double(600, 0, 50, 100));
    walls.add(new Rectangle2D.Double(700, 50, 50, 50));
    walls.add(new Rectangle2D.Double(0, 150, 200, 50));
    walls.add(new Rectangle2D.Double(600, 150, 200, 50));

    walls.add(new Rectangle2D.Double(50, 250, 100, 100));
    walls.add(new Rectangle2D.Double(200, 250, 50, 100));
    walls.add(new Rectangle2D.Double(300, 250, 200, 100));
    walls.add(new Rectangle2D.Double(550, 250, 50, 100));
    walls.add(new Rectangle2D.Double(650, 250, 100, 100));

    walls.add(new Rectangle2D.Double(50, 50+450, 50, 50));
    walls.add(new Rectangle2D.Double(150, 0+500, 50, 100));
    walls.add(new Rectangle2D.Double(250, 0+400, 50, 200));
    walls.add(new Rectangle2D.Double(350, 50+400, 50, 150));
    walls.add(new Rectangle2D.Double(450, 0+400, 100, 200));
    walls.add(new Rectangle2D.Double(600, 0+500, 50, 100));
    walls.add(new Rectangle2D.Double(700, 50+450, 50, 50));
    walls.add(new Rectangle2D.Double(0, 150+250, 200, 50));
    walls.add(new Rectangle2D.Double(600, 150+250, 200, 50));
  }

  private boolean isPointIntersectingPacman(int x, int y) {
    Rectangle2D pacmanBounds = new Rectangle2D.Double(pacmanX, pacmanY, pacSize, pacSize);
    Rectangle2D pointBounds = new Rectangle2D.Double(x, y, 10, 10);
    return pacmanBounds.intersects(pointBounds);
  }

  private boolean isPointIntersectingWall(int x, int y) {
    Rectangle2D pointBounds = new Rectangle2D.Double(x, y, 10, 10);

    for (Rectangle2D wall : walls) {
      if (pointBounds.intersects(wall)) {
        return true;
      }
    }

    return false;
  }

  public static void main(String[] args) {
    JFrame frame = new JFrame("Pacman Game");
    PacmanGame game = new PacmanGame();
    frame.add(game);
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
